#!/usr/bin/env bash

docker stop payment-rabbit
docker run \
    --rm $1 \
    --publish 5672:5672 \
    --name payment-rabbit \
    --hostname payment-rabbit \
    rabbitmq:3.7-alpine