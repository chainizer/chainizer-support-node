#!/usr/bin/env bash

docker stop payment-mongo
docker run \
    --rm $1 \
    --publish 27017:27017 \
    --name payment-mongo \
    --hostname payment-mongo \
    mongo:3.6