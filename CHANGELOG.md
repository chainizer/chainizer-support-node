# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.0.0-alpha.55"></a>
# [1.0.0-alpha.55](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.54...v1.0.0-alpha.55) (2018-08-14)




**Note:** Version bump only for package @chainizer/support

<a name="1.0.0-alpha.54"></a>
# [1.0.0-alpha.54](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.53...v1.0.0-alpha.54) (2018-08-03)


### Features

* upgrade dependencies ([7f89317](https://gitlab.com/chainizer/chainizer-support-node/commit/7f89317))




<a name="1.0.0-alpha.53"></a>
# [1.0.0-alpha.53](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.52...v1.0.0-alpha.53) (2018-06-04)




**Note:** Version bump only for package @chainizer/support

<a name="1.0.0-alpha.52"></a>
# [1.0.0-alpha.52](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.51...v1.0.0-alpha.52) (2018-05-31)


### Bug Fixes

* fix app conf override ([90be9e9](https://gitlab.com/chainizer/chainizer-support-node/commit/90be9e9))




<a name="1.0.0-alpha.51"></a>
# [1.0.0-alpha.51](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.50...v1.0.0-alpha.51) (2018-05-31)


### Features

* rename config stuff ([5373088](https://gitlab.com/chainizer/chainizer-support-node/commit/5373088))




<a name="1.0.0-alpha.50"></a>
# [1.0.0-alpha.50](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.49...v1.0.0-alpha.50) (2018-05-29)


### Bug Fixes

* fix app conf override ([2daecbf](https://gitlab.com/chainizer/chainizer-support-node/commit/2daecbf))




<a name="1.0.0-alpha.49"></a>
# [1.0.0-alpha.49](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.48...v1.0.0-alpha.49) (2018-05-25)


### Bug Fixes

* mocked methods should return promise by default ([48db17b](https://gitlab.com/chainizer/chainizer-support-node/commit/48db17b))




<a name="1.0.0-alpha.48"></a>
# [1.0.0-alpha.48](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.47...v1.0.0-alpha.48) (2018-05-25)


### Bug Fixes

* mocked ack and channel return promise ([5fcb538](https://gitlab.com/chainizer/chainizer-support-node/commit/5fcb538))




<a name="1.0.0-alpha.47"></a>
# [1.0.0-alpha.47](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.46...v1.0.0-alpha.47) (2018-05-25)


### Bug Fixes

* make mocked channel static ([d278f4a](https://gitlab.com/chainizer/chainizer-support-node/commit/d278f4a))
* make mocked channel static ([e5cbc51](https://gitlab.com/chainizer/chainizer-support-node/commit/e5cbc51))




<a name="1.0.0-alpha.46"></a>
# [1.0.0-alpha.46](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.45...v1.0.0-alpha.46) (2018-05-25)


### Reverts

* remove useless health check for mongoose intgration ([30744c2](https://gitlab.com/chainizer/chainizer-support-node/commit/30744c2))




<a name="1.0.0-alpha.45"></a>
# [1.0.0-alpha.45](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.44...v1.0.0-alpha.45) (2018-05-25)


### Bug Fixes

* remove useless health check for mongoose intgration ([3c00a97](https://gitlab.com/chainizer/chainizer-support-node/commit/3c00a97))




<a name="1.0.0-alpha.44"></a>
# [1.0.0-alpha.44](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.43...v1.0.0-alpha.44) (2018-05-25)


### Bug Fixes

* start comsumer only if backlog queue is configured ([5df97d9](https://gitlab.com/chainizer/chainizer-support-node/commit/5df97d9))




<a name="1.0.0-alpha.43"></a>
# [1.0.0-alpha.43](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.42...v1.0.0-alpha.43) (2018-05-25)


### Bug Fixes

* AMQP command consumers can declare backlog and/or delayed command queues ([e7b17d8](https://gitlab.com/chainizer/chainizer-support-node/commit/e7b17d8))




<a name="1.0.0-alpha.42"></a>
# [1.0.0-alpha.42](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.41...v1.0.0-alpha.42) (2018-05-25)


### Bug Fixes

* add default content builder to empty buffer ([cf10bec](https://gitlab.com/chainizer/chainizer-support-node/commit/cf10bec))
* fix management of consumed answers ([e5987a0](https://gitlab.com/chainizer/chainizer-support-node/commit/e5987a0))




<a name="1.0.0-alpha.41"></a>
# [1.0.0-alpha.41](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.40...v1.0.0-alpha.41) (2018-05-25)


### Bug Fixes

* add getCnqAmqp to main api ([b50f5ec](https://gitlab.com/chainizer/chainizer-support-node/commit/b50f5ec))




<a name="1.0.0-alpha.40"></a>
# [1.0.0-alpha.40](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.39...v1.0.0-alpha.40) (2018-05-25)


### Bug Fixes

* fix consume's fake return type ([c913246](https://gitlab.com/chainizer/chainizer-support-node/commit/c913246))




<a name="1.0.0-alpha.39"></a>
# [1.0.0-alpha.39](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.38...v1.0.0-alpha.39) (2018-05-25)


### Bug Fixes

* fix consume's fake return type ([3d51609](https://gitlab.com/chainizer/chainizer-support-node/commit/3d51609))




<a name="1.0.0-alpha.38"></a>
# [1.0.0-alpha.38](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.37...v1.0.0-alpha.38) (2018-05-24)


### Bug Fixes

* fix broken unit test ([b32b946](https://gitlab.com/chainizer/chainizer-support-node/commit/b32b946))


### Features

* add cnq testing tools ([3a933fb](https://gitlab.com/chainizer/chainizer-support-node/commit/3a933fb))
* expose Amqp and setCnqAmqp directly from main ([2532250](https://gitlab.com/chainizer/chainizer-support-node/commit/2532250))




<a name="1.0.0-alpha.37"></a>
# [1.0.0-alpha.37](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.36...v1.0.0-alpha.37) (2018-05-23)


### Features

* handle headers when dealing from AMQP/HTTP requests and task metadata ([1750eb4](https://gitlab.com/chainizer/chainizer-support-node/commit/1750eb4))




<a name="1.0.0-alpha.36"></a>
# [1.0.0-alpha.36](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.35...v1.0.0-alpha.36) (2018-05-23)


### Bug Fixes

* be sure cnq can start even without a proper producer or consumer ([b8d3623](https://gitlab.com/chainizer/chainizer-support-node/commit/b8d3623))




<a name="1.0.0-alpha.35"></a>
# [1.0.0-alpha.35](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.34...v1.0.0-alpha.35) (2018-05-23)


### Bug Fixes

* cnq cannot start if when amqp consumer are not defined ([f48215f](https://gitlab.com/chainizer/chainizer-support-node/commit/f48215f))




<a name="1.0.0-alpha.34"></a>
# [1.0.0-alpha.34](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.33...v1.0.0-alpha.34) (2018-05-23)


### Bug Fixes

* inverse logged port and hostname when app start ([33e185a](https://gitlab.com/chainizer/chainizer-support-node/commit/33e185a))




<a name="1.0.0-alpha.33"></a>
# [1.0.0-alpha.33](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.32...v1.0.0-alpha.33) (2018-05-23)




**Note:** Version bump only for package @chainizer/support

<a name="1.0.0-alpha.32"></a>
# [1.0.0-alpha.32](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.31...v1.0.0-alpha.32) (2018-05-22)


### Bug Fixes

* move lodash to dev dependencies ([ea268a1](https://gitlab.com/chainizer/chainizer-support-node/commit/ea268a1))
* move yargs to dev dependencies ([7de679a](https://gitlab.com/chainizer/chainizer-support-node/commit/7de679a))
* optimize peer dependencies ([f6e492a](https://gitlab.com/chainizer/chainizer-support-node/commit/f6e492a))




<a name="1.0.0-alpha.31"></a>
# [1.0.0-alpha.31](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.30...v1.0.0-alpha.31) (2018-05-22)




**Note:** Version bump only for package @chainizer/support

<a name="1.0.0-alpha.30"></a>
# [1.0.0-alpha.30](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.29...v1.0.0-alpha.30) (2018-05-22)


### Bug Fixes

* Config do not accept unknown key/value pairs ([e6c3571](https://gitlab.com/chainizer/chainizer-support-node/commit/e6c3571))
* fix npm dependencies installation ([9381d54](https://gitlab.com/chainizer/chainizer-support-node/commit/9381d54))
* fix scripts ([902fcea](https://gitlab.com/chainizer/chainizer-support-node/commit/902fcea))
* fix support-yargs module name ([9c8d0a2](https://gitlab.com/chainizer/chainizer-support-node/commit/9c8d0a2))
* fix unit testing ([3878615](https://gitlab.com/chainizer/chainizer-support-node/commit/3878615))


### Features

* add a bean options ([8017742](https://gitlab.com/chainizer/chainizer-support-node/commit/8017742))
* add app life-cycle management based on hooks ([9011e1b](https://gitlab.com/chainizer/chainizer-support-node/commit/9011e1b))
* add CNQ integration ([910878c](https://gitlab.com/chainizer/chainizer-support-node/commit/910878c))
* add express integration ([3798ec7](https://gitlab.com/chainizer/chainizer-support-node/commit/3798ec7))
* add mongoose integration ([0885d3b](https://gitlab.com/chainizer/chainizer-support-node/commit/0885d3b))
* add mongoose integration ([f210f3b](https://gitlab.com/chainizer/chainizer-support-node/commit/f210f3b))
* add server instance to hook's arguments ([4a62c11](https://gitlab.com/chainizer/chainizer-support-node/commit/4a62c11))
* fix config namespace for winston ([851ac03](https://gitlab.com/chainizer/chainizer-support-node/commit/851ac03))
* implement winston integration ([9aac5d4](https://gitlab.com/chainizer/chainizer-support-node/commit/9aac5d4))
* integrate task executor ([cbe4be5](https://gitlab.com/chainizer/chainizer-support-node/commit/cbe4be5))
