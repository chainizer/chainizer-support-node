import 'mocha';
import {expect} from 'chai';
import {config, registerConfig} from '../src/main';

describe('main', () => {

    it('should load config', () => {
        registerConfig('./config/registered.config.json');
        const c = config({
            argv: [
                '--config',
                './config/another.config.json',
                '--set.default.key1',
                'value1_cmd',
                '--set.test.key1',
                'value1_cmd'
            ]
        });

        expect(c).to.have.nested.property('default.key1', 'value1_cmd');
        expect(c).to.have.nested.property('default.key2', 'value2_another');
        expect(c).to.have.nested.property('default.key3', 'value3_default');
        expect(c).to.have.nested.property('default.key4', 'value4_registered');

        expect(c).to.have.nested.property('test.key1', 'value1_cmd');
        expect(c).to.have.nested.property('test.key2', 'value2_another');
        expect(c).to.have.nested.property('test.key3', 'value3_default');
        expect(c).to.have.nested.property('test.key4', 'value4_registered');
    });

    it('should set and get config', () => {
        const nodeEnv = process.env.NODE_ENV;
        process.env.NODE_ENV = '';
        config({clean: true, argv: '--'}).set('set.key1', 'value1');
        expect(config().get('default.key1')).to.be.eq('value1_default');
        expect(config().get('set.key1')).to.be.eq('value1');
        process.env.NODE_ENV = nodeEnv;
    });

});