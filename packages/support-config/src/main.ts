import {join} from 'path';
import {accessSync, constants, existsSync, readFileSync} from 'fs';
import {get, merge, set} from 'lodash';
import {args} from '@chainizer/support-yargs';

const registeredConfFiles = [];

export function registerConfig(path: string) {
    registeredConfFiles.push(path);
}

let loadedConfig;

function processConfig(options: ConfigOptions = {}): Config {
    const argv = args(options);

    const cmdConf = Object.getOwnPropertyNames(argv.set || {})
        .map(k => set({}, k, argv.set[k]));

    const confFiles = [].concat(registeredConfFiles);

    const relativeDefaultConfFile = join('config/default.config.json');
    if (existsSync(relativeDefaultConfFile)) {
        confFiles.push(relativeDefaultConfFile)
    }

    const relativeEnvConfFile = join('config/', `${process.env.NODE_ENV || 'development'}.config.json`);
    if (existsSync(relativeEnvConfFile)) {
        confFiles.push(relativeEnvConfFile)
    }

    const fileConf = confFiles.concat(argv.config)
        .filter(f => f)
        .filter(f => !accessSync(f, constants.R_OK))
        .map(f => JSON.parse(readFileSync(f).toString()));

    let data = merge.apply(null, fileConf.concat(cmdConf));

    data.get = (path: string, def?: any) => {
        return get(data, path, def);
    };

    data.set = (path: string, val?: any) => {
        return set(data, path, val);
    };

    return data;
}

export interface ConfigOptions {
    argv?: string | string[]
    clean?: boolean
}

export function config(options: ConfigOptions = {}): Config {
    if (options.clean) {
        loadedConfig = undefined;
    }
    if (!loadedConfig) {
        loadedConfig = processConfig(options);
    }
    return loadedConfig;
}

export interface Config {
    [key: string]: any

    get: (path: string, def?: any) => any
    set: (path: string, val?: any) => any
}
