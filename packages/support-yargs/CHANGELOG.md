# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.0.0-alpha.55"></a>
# [1.0.0-alpha.55](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.54...v1.0.0-alpha.55) (2018-08-14)




**Note:** Version bump only for package @chainizer/support-yargs

<a name="1.0.0-alpha.54"></a>
# [1.0.0-alpha.54](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.53...v1.0.0-alpha.54) (2018-08-03)


### Features

* upgrade dependencies ([7f89317](https://gitlab.com/chainizer/chainizer-support-node/commit/7f89317))




<a name="1.0.0-alpha.53"></a>
# [1.0.0-alpha.53](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.52...v1.0.0-alpha.53) (2018-06-04)




**Note:** Version bump only for package @chainizer/support-yargs

<a name="1.0.0-alpha.38"></a>
# [1.0.0-alpha.38](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.37...v1.0.0-alpha.38) (2018-05-24)




**Note:** Version bump only for package @chainizer/support-yargs

<a name="1.0.0-alpha.33"></a>
# [1.0.0-alpha.33](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.32...v1.0.0-alpha.33) (2018-05-23)




**Note:** Version bump only for package @chainizer/support-yargs

<a name="1.0.0-alpha.32"></a>
# [1.0.0-alpha.32](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.31...v1.0.0-alpha.32) (2018-05-22)


### Bug Fixes

* move yargs to dev dependencies ([7de679a](https://gitlab.com/chainizer/chainizer-support-node/commit/7de679a))




<a name="1.0.0-alpha.31"></a>
# [1.0.0-alpha.31](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.30...v1.0.0-alpha.31) (2018-05-22)




**Note:** Version bump only for package @chainizer/support-yargs

<a name="1.0.0-alpha.30"></a>
# [1.0.0-alpha.30](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.29...v1.0.0-alpha.30) (2018-05-22)


### Features

* add a bean options ([8017742](https://gitlab.com/chainizer/chainizer-support-node/commit/8017742))
