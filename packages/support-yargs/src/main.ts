let loadedArgs;

export interface ArgsOptions {
    argv?: string | string[]
    clean?: boolean
}

export function args(options: ArgsOptions = {}) {
    if (options.clean) {
        loadedArgs = undefined;
    }
    if (!loadedArgs) {
        loadedArgs = require('yargs')
            .env('PAYMENT_REPOSITORY')
            .option('config', {
                alias: 'c',
                type: 'array'
            })
            .option('set', {
                alias: 's'
            })
            .help()
            .parse(options.argv || process.argv);
    }
    return loadedArgs;
}