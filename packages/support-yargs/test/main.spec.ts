import 'mocha';
import {expect} from 'chai';
import {args} from '../src/main';

describe('main', () => {

    it('should parse arguments', () => {
        const args1 = args({
            argv: '--key1 value1 --key2=value2 -b'
        });
        expect(args1).to.have.property('key1', 'value1');
        expect(args1).to.have.property('key2', 'value2');
        expect(args1).to.have.property('b', true);

        const args2 = args();
        expect(args2).to.have.property('key1', 'value1');
        expect(args2).to.have.property('key2', 'value2');
        expect(args2).to.have.property('b', true);

        const args3 = args({
            clean: true
        });
        expect(args3).to.not.have.property('key1');
        expect(args3).to.not.have.property('key2');
        expect(args3).to.not.have.property('b');
        expect(args3).to.have.property('require', 'ts-node/register');
    });

});