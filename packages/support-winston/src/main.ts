import * as winston from 'winston';
import {ConsoleTransportOptions, DailyRotateFileTransportOptions, LoggerInstance} from 'winston';
import 'winston-daily-rotate-file';
import {config} from '@chainizer/support-config';
import {merge, omit} from 'lodash';

const loggers: {[key in string]: LoggerInstance} = {};

export interface LoggerConfig {
    level: string
    console: ConsoleTransportOptions,
    files: DailyRotateFileTransportOptions[]
}

export interface LoggerOptions {
    name?: string
    clean?: boolean
}

export function logger(options: LoggerOptions = {}): LoggerInstance {
    const opt = merge({
        name: 'default'
    }, options);

    if (!loggers[opt.name]) {
        const transports = [];
        const c: LoggerConfig = config().get(`winston.${opt.name}`, {});

        if (c.console) {
            transports.push(new (winston.transports.Console)(merge({
                humanReadableUnhandledException: true,
                colorize: true,
                timestamp: true
            }, c.console)));
        }

        if (c.files && Array.isArray(c.files)) {
            c.files.forEach(tConfig => {
                transports.push(new (winston.transports.DailyRotateFile)(merge({
                    datePattern: 'YYYY-MM-DD-HH',
                    zippedArchive: true,
                    maxSize: '50m',
                    maxFiles: '30d'
                }, tConfig)));
            });
        }

        loggers[opt.name] = new (winston.Logger)(merge({
                level: 'info',
                transports
            },
            omit(c, ['console', 'files'])
        ));
    }

    return loggers[opt.name];
}
