import 'mocha';
import {expect} from 'chai';
import {logger} from '../src/main';
import {config} from '@chainizer/support-config';

describe('main', () => {

    it('should create default logger', () => {
        config({clean: true});
        const loggerNoConfig = logger();
        loggerNoConfig.info('default');
        expect(loggerNoConfig).to.be.eq(logger());
    });

    it('should create named logger', () => {
        config({clean: true});
        const loggerNoConfig = logger({name: 'named'});
        loggerNoConfig.info('named');
        expect(loggerNoConfig).to.be.eq(logger({name: 'named'}));
    });

    it('should create logger from configuration', () => {
        config({clean: true})
            .set('winston.configuration.console', true)
            .set('winston.configuration.files[0].level', 'info')
            .set('winston.configuration.files[0].name', 'drf-test')
            .set('winston.configuration.files[0].filename', './logs/test-%DATE%.log');
        const loggerNoConfig = logger({name: 'configuration'});
        loggerNoConfig.info('configuration');
        expect(loggerNoConfig).to.be.eq(logger({name: 'configuration'}));
    });

});