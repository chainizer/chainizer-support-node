import {logger} from '@chainizer/support-winston';

export interface Metadata {
    messageId?: string;
    transactionId?: string;
    headers?: {
        [key: string]: any
    };

    [key: string]: any;
}

export interface Task {
    (payload: any, metadata: Metadata): Promise<any>
}

export enum TaskType {
    command = 'command',
    query = 'query'
}

interface Registry {
    [key: string]: {
        [key: string]: Task
    }
}

let registry: Registry = {};

export function registerTask(type: TaskType, name: string, task: Task) {
    if (!registry[type]) {
        registry[type] = {};
    }
    registry[type][name] = task;
    logger().info('register %s/%s', type, name);
    return {
        and: registerTask
    };
}

export function cleanRegisteredTasks() {
    registry = {};
}

export class TaskNotFound extends Error {
    public status = 404;
    public name = 'TaskNotFound';

    constructor(type: string, name: string) {
        super(`unable to find the task ${type}/${name}`);
    }
}

export class TaskFailed extends Error {
    public status = 500;
    public name = 'TaskFailed';

    constructor(type: string, name: string, public cause: Error) {
        super(`the task ${type}/${name} failed because ${cause.name}/${cause.message}`);
    }
}

export async function executeTask(type: TaskType, name: string, payload: any, metadata: Metadata): Promise<any> {
    if (!registry[type] || !registry[type][name]) {
        throw new TaskNotFound(type, name);
    }
    const l = taskLogger(metadata);
    const task = registry[type][name];
    l.info('execute task [%s/%s]', type, name);
    l.debug('Metadata%s', JSON.stringify(metadata));
    l.profile('execute task');
    try {
        return await task(payload, metadata);
    } catch (e) {
        l.info('task failed [%s/%s]', e.name, e.message);
        if (e.status) {
            throw e;
        }
        throw new TaskFailed(type, name, e);
    } finally {
        l.profile('execute task');
    }
}

function applyLogging(logger, fn: string, prefix: string, args: any[]) {
    if (typeof args[0] === 'string') {
        const first = args.shift();
        args.unshift(prefix + first);
    } else {
        args.unshift(prefix);
    }
    logger[fn].apply(logger, args);
}

export function taskLogger(metadata: Metadata) {
    const L = logger();
    const prefixAsArray = [];
    if (metadata.messageId) {
        prefixAsArray.push(metadata.messageId);
    }
    if (metadata.transactionId) {
        prefixAsArray.push(metadata.transactionId);
    }
    const prefixAsString = prefixAsArray.join('|');
    const prefix = prefixAsString ? '[' + prefixAsString + '] - ' : '';
    return {
        debug(...args: any[]) {
            applyLogging(L, 'debug', prefix, args);
        },
        info(...args: any[]) {
            applyLogging(L, 'info', prefix, args);
        },
        warn(...args: any[]) {
            applyLogging(L, 'warn', prefix, args);
        },
        error(...args: any[]) {
            applyLogging(L, 'error', prefix, args);
        },
        profile(...args: any[]) {
            applyLogging(L, 'profile', prefix, args);
        }
    };
}
