import {expect} from 'chai';
import 'mocha';
import * as moment from 'moment';
import {config} from '@chainizer/support-config';
import {cleanRegisteredTasks, executeTask, Metadata, registerTask, taskLogger, TaskType} from '../src/main';

describe('cnq.executor', () => {
    let metadata: Metadata;

    beforeEach(() => {
        config().set('winston.default.console', true);
        metadata = {
            messageId: `msg-${Date.now()}`,
            createdAt: moment().toISOString(),
            transactionId: ''
        };
        cleanRegisteredTasks();
        registerTask(TaskType.command, 'cmd-ok', p => Promise.resolve(p));
        registerTask(TaskType.command, 'cmd-ko', async (p, m) => {
            const error = new Error('an error');
            taskLogger(m).warn('will trow an error');
            taskLogger(m).error(error);
            throw error;
        });
        registerTask(TaskType.command, 'cmd-ko-401', () => {
            const e = new Error('a custom error');
            e['status'] = 401;
            return Promise.reject(e);
        });
        registerTask(TaskType.query, 'qry-ok', p => Promise.resolve(p));
    });

    it('should failed when command not registered', async () => {
        try {
            await executeTask(TaskType.command, 'cmd-unknown', null, metadata)
        } catch (e) {
            expect(e.status).to.be.eq(404)
        }
    });

    it('should failed when command failed', async () => {
        try {
            await executeTask(TaskType.command, 'cmd-ko', null, metadata)
        } catch (e) {
            expect(e.status).to.be.eq(500)
        }
    });

    it('should failed when command failed with custom error', async () => {
        try {
            await executeTask(TaskType.command, 'cmd-ko-401', null, metadata)
        } catch (e) {
            expect(e.status).to.be.eq(401)
        }
    });

    it('should succeed when command succeed', async () => {
        const r = await executeTask(TaskType.query, 'qry-ok', true, metadata);
        expect(r).to.be.true;
    });

    it('should succeed with no messageId', async () => {
        const r = await executeTask(TaskType.query, 'qry-ok', true, {
            transactionId: 'transactionId'
        });
        expect(r).to.be.true;
    });

});
