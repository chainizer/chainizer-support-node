import {config} from '@chainizer/support-config';
import {Options} from 'amqplib/properties';
import {expect} from 'chai';
import 'mocha';
import {FakeCnq} from '../src/main';

config().set('cnq.amqp.connection', 'test');

describe('main', () => {
    const f = new FakeCnq();

    afterEach(() => {
        f.reset();
    });

    it('should publish', () => {
        const oExchange1 = 'exchange1';
        const oExchange2 = 'exchange2';
        const oRoutingKey = 'routingKey';
        const oContent = Buffer.from('');
        const oPublishOptions = {};
        f.handle(
            0,
            (exchange: string, routingKey: string, content: Buffer, options?: Options.Publish) => {
                expect(exchange).to.be.eq(oExchange1);
                expect(routingKey).to.be.eq(oRoutingKey);
                expect(content).to.be.eq(oContent);
                expect(options).to.be.eq(oPublishOptions);
            }
        );
        f.handle(
            1
        );
        f.channel.publish(oExchange1, oRoutingKey, oContent, oPublishOptions);
        f.channel.publish(oExchange2, oRoutingKey, oContent, oPublishOptions);
        f.channel.publish.getCall(1).calledWith(oExchange2, oRoutingKey, oContent, oPublishOptions);
    });

    it('should consume', async () => {
        const queue = 'queue';
        const oContent = Buffer.from('');
        f.handle(
            0,
            undefined,
            () => oContent
        );
        f.handle(
            1
        );
        await f.channel.consume(queue, (message) => {
            expect(message).to.have.nested.property('content', oContent);
        });
        const consumer = await f.channel.consume(queue, (message) => {
            expect(message).to.have.nested.property('content');
        });
        expect(consumer).to.exist;
    });

    it('should publish and consume', done => {
        const oExchange1 = 'exchange1';
        const oRoutingKey = 'routingKey';
        const oContent = Buffer.from('');
        const oPublishOptions = {
            messageId: 'messageId'
        };
        const queue = 'queue';
        f.handle(
            0,
            (exchange: string, routingKey: string, content: Buffer, options?: Options.Publish) => {
                expect(exchange).to.be.eq(oExchange1);
                expect(routingKey).to.be.eq(oRoutingKey);
                expect(content).to.be.eq(oContent);
                expect(options).to.be.eq(oPublishOptions);
            },
            () => oContent
        );
        f.channel.consume(queue, (message) => {
            expect(message).to.have.nested.property('properties.correlationId', oPublishOptions.messageId);
            done();
        });
        f.channel.publish(oExchange1, oRoutingKey, oContent, oPublishOptions);
    });

});