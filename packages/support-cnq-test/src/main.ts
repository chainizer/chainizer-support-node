import {Amqp, setCnqAmqp} from '@chainizer/support-cnq';
import {Channel, Connection} from 'amqplib';
import * as sinon from 'sinon';

export interface VerifyPublish {
    (exchange, routingKey, content, options): void
}

export interface BuildConsumedContent {
    (content, options): Buffer
}

const mockedChannel = {
    publish: sinon.stub().returns(Promise.resolve({})),
    consume: sinon.stub().returns(Promise.resolve({})),
    ack: sinon.stub().returns(Promise.resolve({})),
    cancel: sinon.stub().returns(Promise.resolve({}))
};


export class FakeCnq {

    constructor(public channel = mockedChannel) {
        setCnqAmqp(new Amqp(<Connection> {}, <Channel>  mockedChannel));
    }

    reset() {
        mockedChannel.publish.reset();
        mockedChannel.consume.reset();
    }

    handle(
        nth: number,
        verifyPublish: VerifyPublish = undefined,
        buildConsumedContent: BuildConsumedContent = () => new Buffer('')
    ) {
        let _content;
        let _options;
        mockedChannel.publish.onCall(nth).callsFake((exchange, routingKey, content, options) => {
            _content = content;
            _options = options;
            if (verifyPublish) {
                verifyPublish(exchange, routingKey, content, options);
            }
        });
        mockedChannel.consume.onCall(nth).callsFake(function (queue, cb) {
            setTimeout(() => {
                cb({
                    properties: {
                        correlationId: _options && _options.messageId,
                        headers: {
                            'executor-status': 200
                        }
                    },
                    content: buildConsumedContent ? buildConsumedContent(_content, _options) : undefined
                });
            }, 0);
            return Promise.resolve({});
        });
    }

    static create() {
        return new FakeCnq();
    }

}