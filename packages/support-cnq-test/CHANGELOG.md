# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.0.0-alpha.55"></a>
# [1.0.0-alpha.55](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.54...v1.0.0-alpha.55) (2018-08-14)




**Note:** Version bump only for package @chainizer/support-cnq-test

<a name="1.0.0-alpha.54"></a>
# [1.0.0-alpha.54](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.53...v1.0.0-alpha.54) (2018-08-03)


### Features

* upgrade dependencies ([7f89317](https://gitlab.com/chainizer/chainizer-support-node/commit/7f89317))




<a name="1.0.0-alpha.53"></a>
# [1.0.0-alpha.53](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.52...v1.0.0-alpha.53) (2018-06-04)




**Note:** Version bump only for package @chainizer/support-cnq-test

<a name="1.0.0-alpha.52"></a>
# [1.0.0-alpha.52](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.51...v1.0.0-alpha.52) (2018-05-31)


### Bug Fixes

* fix app conf override ([90be9e9](https://gitlab.com/chainizer/chainizer-support-node/commit/90be9e9))




<a name="1.0.0-alpha.51"></a>
# [1.0.0-alpha.51](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.50...v1.0.0-alpha.51) (2018-05-31)




**Note:** Version bump only for package @chainizer/support-cnq-test

<a name="1.0.0-alpha.50"></a>
# [1.0.0-alpha.50](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.49...v1.0.0-alpha.50) (2018-05-29)




**Note:** Version bump only for package @chainizer/support-cnq-test

<a name="1.0.0-alpha.49"></a>
# [1.0.0-alpha.49](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.48...v1.0.0-alpha.49) (2018-05-25)


### Bug Fixes

* mocked methods should return promise by default ([48db17b](https://gitlab.com/chainizer/chainizer-support-node/commit/48db17b))




<a name="1.0.0-alpha.48"></a>
# [1.0.0-alpha.48](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.47...v1.0.0-alpha.48) (2018-05-25)


### Bug Fixes

* mocked ack and channel return promise ([5fcb538](https://gitlab.com/chainizer/chainizer-support-node/commit/5fcb538))




<a name="1.0.0-alpha.47"></a>
# [1.0.0-alpha.47](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.46...v1.0.0-alpha.47) (2018-05-25)


### Bug Fixes

* make mocked channel static ([d278f4a](https://gitlab.com/chainizer/chainizer-support-node/commit/d278f4a))
* make mocked channel static ([e5cbc51](https://gitlab.com/chainizer/chainizer-support-node/commit/e5cbc51))




<a name="1.0.0-alpha.44"></a>
# [1.0.0-alpha.44](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.43...v1.0.0-alpha.44) (2018-05-25)




**Note:** Version bump only for package @chainizer/support-cnq-test

<a name="1.0.0-alpha.43"></a>
# [1.0.0-alpha.43](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.42...v1.0.0-alpha.43) (2018-05-25)




**Note:** Version bump only for package @chainizer/support-cnq-test

<a name="1.0.0-alpha.42"></a>
# [1.0.0-alpha.42](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.41...v1.0.0-alpha.42) (2018-05-25)


### Bug Fixes

* add default content builder to empty buffer ([cf10bec](https://gitlab.com/chainizer/chainizer-support-node/commit/cf10bec))




<a name="1.0.0-alpha.41"></a>
# [1.0.0-alpha.41](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.40...v1.0.0-alpha.41) (2018-05-25)




**Note:** Version bump only for package @chainizer/support-cnq-test

<a name="1.0.0-alpha.40"></a>
# [1.0.0-alpha.40](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.39...v1.0.0-alpha.40) (2018-05-25)


### Bug Fixes

* fix consume's fake return type ([c913246](https://gitlab.com/chainizer/chainizer-support-node/commit/c913246))




<a name="1.0.0-alpha.39"></a>
# [1.0.0-alpha.39](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.38...v1.0.0-alpha.39) (2018-05-25)


### Bug Fixes

* fix consume's fake return type ([3d51609](https://gitlab.com/chainizer/chainizer-support-node/commit/3d51609))




<a name="1.0.0-alpha.38"></a>
# [1.0.0-alpha.38](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.37...v1.0.0-alpha.38) (2018-05-24)


### Features

* add cnq testing tools ([3a933fb](https://gitlab.com/chainizer/chainizer-support-node/commit/3a933fb))
