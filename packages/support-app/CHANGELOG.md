# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.0.0-alpha.55"></a>
# [1.0.0-alpha.55](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.54...v1.0.0-alpha.55) (2018-08-14)




**Note:** Version bump only for package @chainizer/support-app

<a name="1.0.0-alpha.54"></a>
# [1.0.0-alpha.54](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.53...v1.0.0-alpha.54) (2018-08-03)


### Features

* upgrade dependencies ([7f89317](https://gitlab.com/chainizer/chainizer-support-node/commit/7f89317))




<a name="1.0.0-alpha.53"></a>
# [1.0.0-alpha.53](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.52...v1.0.0-alpha.53) (2018-06-04)




**Note:** Version bump only for package @chainizer/support-app

<a name="1.0.0-alpha.50"></a>
# [1.0.0-alpha.50](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.49...v1.0.0-alpha.50) (2018-05-29)


### Bug Fixes

* fix app conf override ([2daecbf](https://gitlab.com/chainizer/chainizer-support-node/commit/2daecbf))




<a name="1.0.0-alpha.38"></a>
# [1.0.0-alpha.38](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.37...v1.0.0-alpha.38) (2018-05-24)




**Note:** Version bump only for package @chainizer/support-app

<a name="1.0.0-alpha.34"></a>
# [1.0.0-alpha.34](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.33...v1.0.0-alpha.34) (2018-05-23)


### Bug Fixes

* inverse logged port and hostname when app start ([33e185a](https://gitlab.com/chainizer/chainizer-support-node/commit/33e185a))




<a name="1.0.0-alpha.33"></a>
# [1.0.0-alpha.33](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.32...v1.0.0-alpha.33) (2018-05-23)




**Note:** Version bump only for package @chainizer/support-app

<a name="1.0.0-alpha.32"></a>
# [1.0.0-alpha.32](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.31...v1.0.0-alpha.32) (2018-05-22)




**Note:** Version bump only for package @chainizer/support-app

<a name="1.0.0-alpha.31"></a>
# [1.0.0-alpha.31](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.30...v1.0.0-alpha.31) (2018-05-22)




**Note:** Version bump only for package @chainizer/support-app

<a name="1.0.0-alpha.30"></a>
# [1.0.0-alpha.30](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.29...v1.0.0-alpha.30) (2018-05-22)


### Features

* add app life-cycle management based on hooks ([9011e1b](https://gitlab.com/chainizer/chainizer-support-node/commit/9011e1b))
* add express integration ([3798ec7](https://gitlab.com/chainizer/chainizer-support-node/commit/3798ec7))
* add server instance to hook's arguments ([4a62c11](https://gitlab.com/chainizer/chainizer-support-node/commit/4a62c11))
