import 'mocha';
import {expect} from 'chai';
import {spy} from 'sinon';
import {clearHooks, getExpressApp, getHooks, getServer, HookType, registerHook, startApp} from '../src/main';
import {config} from '@chainizer/support-config';

describe('main', () => {

    beforeEach(() => {
        clearHooks();
    });

    it('should register hooks', () => {
        registerHook(HookType.startup, async () => {
        });
        registerHook(HookType.health, async () => {
        });
        registerHook(HookType.shutdown, async () => {
        });
        expect(getHooks().startup).to.have.lengthOf(1);
        expect(getHooks().health).to.have.lengthOf(1);
        expect(getHooks().shutdown).to.have.lengthOf(1);
    });

    it('should start app', async () => {
        config().set('logging.default.console', true);

        const startupHook = spy();
        registerHook(HookType.startup, startupHook);

        const healthHook = spy();
        registerHook(HookType.health, healthHook);

        const shutdownHook = spy();
        registerHook(HookType.shutdown, shutdownHook);

        const server = await startApp();

        expect(startupHook.called).to.be.true;
        // expect(healthHook.called).to.be.true;
        // expect(shutdownHook.called).to.be.true;

        return new Promise(resolve => server.close(() => resolve()));
    });

    it('should return express app', () => {
        expect(getExpressApp()).to.exist;
    });

    it('should return server', () => {
        expect(getServer()).to.exist;
    });

});