import {config} from '@chainizer/support-config';
import {logger} from '@chainizer/support-winston';
import * as terminus from '@godaddy/terminus';
import * as express from 'express';
import {Express} from 'express';
import {createServer, Server} from 'http';

export enum HookType {
    startup = 'startup',
    health = 'health',
    shutdown = 'shutdown'
}

export interface Hook {
    (): Promise<any>
}

const hooks: {[id in HookType]: Hook[]} = {
    startup: [],
    health: [],
    shutdown: []
};

const expressApp = express();
const server = createServer(expressApp);

export function getExpressApp(): Express {
    return expressApp;
}

export function getServer(): Server {
    return server;
}

export function getHooks() {
    return hooks;
}

export function clearHooks() {
    hooks.startup = [];
    hooks.health = [];
    hooks.shutdown = [];
}

export function registerHook(type: HookType, hook: Hook) {
    hooks[type].push(hook);
}

export async function startApp(): Promise<Server> {
    for (const hook of <Hook[]> hooks.startup) {
        await hook();
    }

    terminus(server, {
        signal: 'SIGINT',
        healthChecks: {
            '/healthz': async (): Promise<any> => {
                logger().debug('check app health');
                for (const hook of <Hook[]> hooks.health) {
                    await hook();
                }
            }
        },
        async onSignal(): Promise<void> {
            logger().debug('start app shutdown');
            for (const hook of <Hook[]> hooks.shutdown) {
                await hook();
            }
        },
        async onShutdown(): Promise<void> {
            logger().info('app shutdown done');
        }
    });

    const port = config().get('app.port', 3000);
    const hostname = config().get('app.port', '0.0.0.0');

    return new Promise<Server>(resolve => {
        server.listen(port, hostname, () => {
            logger().info('app listening on %s:%s', hostname, port);
            resolve(server);
        });
    })
}