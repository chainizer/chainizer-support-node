# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.0.0-alpha.55"></a>
# [1.0.0-alpha.55](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.54...v1.0.0-alpha.55) (2018-08-14)




**Note:** Version bump only for package @chainizer/support-mongoose

<a name="1.0.0-alpha.54"></a>
# [1.0.0-alpha.54](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.53...v1.0.0-alpha.54) (2018-08-03)


### Features

* upgrade dependencies ([7f89317](https://gitlab.com/chainizer/chainizer-support-node/commit/7f89317))




<a name="1.0.0-alpha.53"></a>
# [1.0.0-alpha.53](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.52...v1.0.0-alpha.53) (2018-06-04)




**Note:** Version bump only for package @chainizer/support-mongoose

<a name="1.0.0-alpha.50"></a>
# [1.0.0-alpha.50](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.49...v1.0.0-alpha.50) (2018-05-29)




**Note:** Version bump only for package @chainizer/support-mongoose

<a name="1.0.0-alpha.46"></a>
# [1.0.0-alpha.46](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.45...v1.0.0-alpha.46) (2018-05-25)


### Reverts

* remove useless health check for mongoose intgration ([30744c2](https://gitlab.com/chainizer/chainizer-support-node/commit/30744c2))




<a name="1.0.0-alpha.45"></a>
# [1.0.0-alpha.45](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.44...v1.0.0-alpha.45) (2018-05-25)


### Bug Fixes

* remove useless health check for mongoose intgration ([3c00a97](https://gitlab.com/chainizer/chainizer-support-node/commit/3c00a97))




<a name="1.0.0-alpha.38"></a>
# [1.0.0-alpha.38](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.37...v1.0.0-alpha.38) (2018-05-24)


### Bug Fixes

* fix broken unit test ([b32b946](https://gitlab.com/chainizer/chainizer-support-node/commit/b32b946))




<a name="1.0.0-alpha.34"></a>
# [1.0.0-alpha.34](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.33...v1.0.0-alpha.34) (2018-05-23)




**Note:** Version bump only for package @chainizer/support-mongoose

<a name="1.0.0-alpha.33"></a>
# [1.0.0-alpha.33](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.32...v1.0.0-alpha.33) (2018-05-23)




**Note:** Version bump only for package @chainizer/support-mongoose

<a name="1.0.0-alpha.32"></a>
# [1.0.0-alpha.32](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.31...v1.0.0-alpha.32) (2018-05-22)




**Note:** Version bump only for package @chainizer/support-mongoose

<a name="1.0.0-alpha.31"></a>
# [1.0.0-alpha.31](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.30...v1.0.0-alpha.31) (2018-05-22)




**Note:** Version bump only for package @chainizer/support-mongoose

<a name="1.0.0-alpha.30"></a>
# [1.0.0-alpha.30](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.29...v1.0.0-alpha.30) (2018-05-22)


### Bug Fixes

* fix unit testing ([3878615](https://gitlab.com/chainizer/chainizer-support-node/commit/3878615))


### Features

* add mongoose integration ([0885d3b](https://gitlab.com/chainizer/chainizer-support-node/commit/0885d3b))
* add mongoose integration ([f210f3b](https://gitlab.com/chainizer/chainizer-support-node/commit/f210f3b))
