import * as mongoose from 'mongoose';
import {ConnectionOptions} from 'mongoose';
import {config} from '@chainizer/support-config';
import {logger} from '@chainizer/support-winston';
import {HookType, registerHook} from '@chainizer/support-app';

export * from 'mongoose';

export interface MongooseConfig {
    url: string
    options: ConnectionOptions
}

mongoose.connection.once('open', () => logger().info('mongo connection opened'));
mongoose.connection.on('error', error => logger().error('mongo connection error', error));
mongoose.connection.on('connecting', () => logger().info('mongo connection connecting'));
mongoose.connection.on('connected', () => logger().info('mongo connection connected'));
mongoose.connection.on('disconnecting', () => logger().info('mongo connection disconnecting'));
mongoose.connection.on('disconnected', () => logger().info('mongo connection disconnected'));

registerHook(HookType.startup, async () => {
    const c: MongooseConfig = config().get('mongoose', {});

    await mongoose.connect(c.url, c.options);
});

registerHook(HookType.health, async () => {
    logger().debug('ping mongoose');
    if (mongoose.connection.readyState === 0) {
        return Promise.reject(new Error('connection disconnected'));
    }
    await mongoose.connection.db.admin().ping();
});

registerHook(HookType.shutdown, async () => {
    logger().info('disconnect mongoose');
    return new Promise<void>((resolve, reject) => {
        mongoose.disconnect(error => {
            if (error) {
                reject(error);
            } else {
                resolve();
            }
        });
    });
});

export function getMongoose() {
    return mongoose;
} 