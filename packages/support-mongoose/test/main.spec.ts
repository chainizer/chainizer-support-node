import 'mocha';
import {expect} from 'chai';
import {getMongoose} from '../src/main'
import {getHooks} from '@chainizer/support-app'
import {registerConfig} from '@chainizer/support-config';

registerConfig(__dirname + '/../config/testing.config.json');

describe('main', () => {
    
    it('should handle mongoose connection', async () => {
        await getHooks().startup[0]();
        await getHooks().health[0]();
        await getHooks().shutdown[0]();
        try {
            await getHooks().health[0]();
            expect(true).to.be.false;
        } catch (e) {
            expect(e).to.exist;
        }
        try {
            await getHooks().shutdown[0]();
            expect(true).to.be.false;
        } catch (e) {
            expect(e).to.exist;
        }
    });

    it('should get mongoose', async () => {
        expect(getMongoose()).to.exist;
    });
});