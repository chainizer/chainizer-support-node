import {Channel, Message} from 'amqplib';
import {camelCase, fromPairs, merge} from 'lodash';
import * as moment from 'moment';
import {v4 as uuid} from 'uuid';
import {logger} from '@chainizer/support-winston';
import {executeTask, Metadata, taskLogger, TaskType} from '@chainizer/support-executor';
import {config} from './config';
import {Amqp, getCnqAmqp} from './amqp';

function fromMessageToMetadata(message: Message): Metadata {
    return merge({
        messageId: message.properties.messageId || uuid(),
        createdAt: message.properties.timestamp || moment().toISOString(),
        transactionId: ''
    }, fromPairs(
        Object.getOwnPropertyNames(message.properties.headers)
            .filter(k => k.startsWith('executor-'))
            .map(k => {
                const key = camelCase(k.replace('executor-', ''));
                const value = message.properties.headers[k];
                return [key, value]
            })
    ), {
        headers: fromPairs(
            Object.getOwnPropertyNames(message.properties.headers)
                .filter(k => !k.startsWith('executor-'))
                .map(k => {
                    const key = camelCase(k.replace('executor-', ''));
                    const value = message.properties.headers[k];
                    return [key, value]
                })
        )
    });
}

async function handleMessage(taskType: TaskType, channel: Channel, message: Message) {
    const taskName = message.properties.type;
    const content = message.content.toString(message.properties.contentEncoding);
    const payload = content && JSON.parse(content);
    const metadata = fromMessageToMetadata(message);
    const L = taskLogger(metadata);
    const replyType = taskType === TaskType.command ? 'CommandReply' : 'QueryReply';
    try {
        const result = await executeTask(taskType, taskName, payload, metadata);
        await handleReply(channel, message, replyType, 200, result);
    } catch (e) {
        const error = e.cause && e.cause.status ? e.cause : e;
        await handleReply(channel, message, replyType, error.status, {
            name: error.name,
            message: error.message
        });
    } finally {
        try {
            channel.ack(message, false);
        } catch (e) {
            L.warn('unable to ack the message', e)
        }
    }
}

async function handleReply(channel: Channel, message: Message, type: string, status: number, payload: any) {
    if (message.properties.replyTo) {
        const amqp = await getCnqAmqp();
        channel.publish('', message.properties.replyTo, new Buffer(JSON.stringify(payload), 'utf8'), {
            messageId: uuid(),
            correlationId: message.properties.messageId,
            type: type,
            contentEncoding: 'utf8',
            contentType: 'application/json',
            appId: amqp.replyToQueue,
            headers: {
                'executor-status': status
            }
        });
    }
}

export async function configureAmqpConsumer(): Promise<Amqp> {
    const amqp = await getCnqAmqp();
    const consumerConfig = config().cnq.consumer;

    if (consumerConfig && consumerConfig.amqp) {
        if (consumerConfig.amqp.command && consumerConfig.amqp.command.backlogQueue) {
            logger().info('start the AMQP command handlers');
            await amqp.channel.consume(
                consumerConfig.amqp.command.backlogQueue,
                async message => handleMessage(TaskType.command, amqp.channel, message)
            );
        }
        if (consumerConfig.amqp.query && consumerConfig.amqp.query.backlogQueue) {
            logger().info('start the AMQP query handlers');
            await amqp.channel.consume(
                consumerConfig.amqp.query.backlogQueue,
                async message => handleMessage(TaskType.query, amqp.channel, message)
            );
        }
    }

    return amqp;
}
