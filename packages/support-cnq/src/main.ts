import {getCnqAmqp} from './amqp';
import {config} from './config';
import {configureAmqpConsumer} from './consumer';
import {HookType, registerHook} from '@chainizer/support-app';
import {configureRestConsumer} from './rest';

export {Amqp, setCnqAmqp, getCnqAmqp} from './amqp';
export {dispatchTask} from './producer';

registerHook(HookType.startup, async () => {
    const amqpConfig = config().cnq.amqp;
    if (amqpConfig) {
        await configureAmqpConsumer();
    }

    const consumerConfig = config().cnq.consumer;
    if (consumerConfig && consumerConfig.rest) {
        configureRestConsumer();
    }
});

registerHook(HookType.health, async () => {
    const amqpConfig = config().cnq.amqp;
    if (amqpConfig) {
        const amqp = await getCnqAmqp();
        await amqp.ping();
    }
});

registerHook(HookType.shutdown, async () => {
    const amqpConfig = config().cnq.amqp;
    if (amqpConfig) {
        const amqp = await getCnqAmqp();
        await amqp.destroy();
    }
});
