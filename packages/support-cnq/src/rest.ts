import {camelCase, fromPairs, merge} from 'lodash';
import {Express, Request, Router} from 'express';
import {json, urlencoded} from 'body-parser';
import {v4 as uuid} from 'uuid';
import * as moment from 'moment';
import {executeTask, Metadata, TaskType} from '@chainizer/support-executor';
import {logger} from '@chainizer/support-winston';
import {getExpressApp} from '@chainizer/support-app';

let router: Router;

function fromRequestToMetadata(req: Request): Metadata {
    return merge({
        messageId: uuid(),
        createdAt: moment().toISOString(),
        transactionId: ''
    }, fromPairs(
        Object.getOwnPropertyNames(req.headers)
            .filter(k => k.startsWith('executor-'))
            .map(k => {
                const key = camelCase(k.replace('executor-', ''));
                const value = req.headers[k];
                return [key, value]
            })
    ), {
        headers: fromPairs(
            Object.getOwnPropertyNames(req.headers)
                .filter(k => !k.startsWith('executor-'))
                .map(k => {
                    const key = camelCase(k.replace('executor-', ''));
                    const value = req.headers[k];
                    return [key, value]
                })
        )
    });
}

function getRoutes(): Router {
    if (!router) {
        logger().info('setup the command&query REST API');

        router = Router();

        router.use(urlencoded({extended: false}));
        router.use(json());

        router.post('/command/:name', async (req, res) => {
            res.setHeader('Content-Type', 'application/json');

            const cmdName = req.params.name;
            const payload = req.body;
            const metadata = fromRequestToMetadata(req);

            try {
                const result = await executeTask(TaskType.command, cmdName, payload, metadata);
                res.status(200).send(result);
            } catch (e) {
                const {status, name, message} = e.cause && e.cause.status ? e.cause : e;
                res.status(status).send({name, message});
            }
        });

        router.get('/query/:name', async (req, res) => {
            res.setHeader('Content-Type', 'application/json');

            const qryName = req.params.name;
            const payload = req.query;
            const metadata = fromRequestToMetadata(req);

            try {
                const result = await executeTask(TaskType.query, qryName, payload, metadata);
                res.status(200).send(result);
            } catch (e) {
                const {status, name, message} = e.cause && e.cause.status ? e.cause : e;
                res.status(status).send({name, message});
            }
        });
    }
    return router;
}

export function configureRestConsumer(): Express {
    return getExpressApp().use('/api', getRoutes());
}
