import {Options} from 'amqplib/properties';
import {Config, config as oConfig, registerConfig} from '@chainizer/support-config';
import {join} from "path";

registerConfig(join(__dirname, '../config/builtin.config.json'));

export interface AmqpConfig {
    appId: string
    connection: string | Options.Connect
    socket: any
}

export interface AmqpConsumerConfig {
    command?: {
        exchange: string
        backlogQueue: string
        backlogRoutingKey: string
        delayedQueue: string
        delayedRoutingKey: string
        defaultDelay: number
        invalidQueue: string
        invalidRoutingKey: string
    }
    query?: {
        exchange: string
        backlogQueue: string
        backlogRoutingKey: string
        invalidQueue: string
        invalidRoutingKey: string
    }
}

export interface AmqpProducerConfig {
    command?: {
        [key in string]: {
            exchange: string
            routingKey: string
        }
    }
    query?: {
        [key in string]: {
            exchange: string
            routingKey: string
        }
    }
}

export interface CnqConfig extends Config {
    cnq: {
        amqp?: AmqpConfig
        consumer?: {
            amqp?: AmqpConsumerConfig,
            rest: boolean
        }
        producer?: {
            amqp: AmqpProducerConfig
            timeout: number
        }
    }
}

export function config(): CnqConfig {
    return <CnqConfig> oConfig()
}