import {Channel, connect, Connection} from 'amqplib';
import {v4 as uuid} from 'uuid';
import {logger} from '@chainizer/support-winston';
import {config} from './config';

export class Amqp {
    public appId: string;
    public replyToQueue: string;

    constructor(
        public connection: Connection,
        public channel: Channel) {
        this.connection = connection;
        this.channel = channel;
        this.appId = config().cnq.amqp.appId || uuid();
        this.replyToQueue = `reply-to-${this.appId}`;
    }

    async ping() {
        const cnqConfig = config().cnq;
        if (cnqConfig.consumer && cnqConfig.consumer.amqp) {
            const consumerConfig = cnqConfig.consumer.amqp;
            if (consumerConfig.command) {
                if (consumerConfig.command.backlogQueue) {
                    await this.channel.checkQueue(consumerConfig.command.backlogQueue);
                }
                if (consumerConfig.command.delayedQueue) {
                    await this.channel.checkQueue(consumerConfig.command.delayedQueue);
                }
                if (consumerConfig.command.backlogQueue || consumerConfig.command.delayedQueue) {
                    await this.channel.checkQueue(consumerConfig.command.invalidQueue);
                }
            }
            if (consumerConfig.query) {
                await this.channel.checkQueue(consumerConfig.query.backlogQueue);
                await this.channel.checkQueue(consumerConfig.query.invalidQueue);
            }
        }
        if (cnqConfig.producer && cnqConfig.producer.amqp) {
            await this.channel.checkQueue(this.replyToQueue);
        }
    }

    async destroy() {
        const cnqConfig = config().cnq;
        if (cnqConfig.producer && cnqConfig.producer.amqp) {
            try {
                await this.channel.deleteQueue(this.replyToQueue);
            } catch (e) {
                logger().warn('unable to delete the reply queue', e);
            }
        }
        try {
            amqp = null;
            await this.connection.close();
        } catch (e) {
            logger().warn('unable to close the connection', e);
        }
    }

}

let amqp: Amqp;

export function setCnqAmqp(value: Amqp) {
    amqp = value;
}

export async function getCnqAmqp(): Promise<Amqp> {
    if (!amqp) {
        logger().info('setup the command&query AMQP API');

        const cnqConfig = config().cnq;
        const connection = await connect(cnqConfig.amqp.connection, cnqConfig.amqp.socket);
        const channel = await connection.createChannel();

        amqp = new Amqp(
            connection,
            channel
        );

        if (cnqConfig.consumer && cnqConfig.consumer.amqp) {
            const consumerConfig = cnqConfig.consumer.amqp;
            if (consumerConfig.command) {
                logger().info('setup the command AMQP API');
                if (consumerConfig.command.backlogQueue || consumerConfig.command.delayedQueue) {
                    await channel.assertExchange(consumerConfig.command.exchange, 'direct', {
                        durable: true
                    });

                    await channel.assertQueue(consumerConfig.command.invalidQueue, {
                        exclusive: false,
                        durable: true,
                        autoDelete: false
                    });
                    await channel.bindQueue(
                        consumerConfig.command.invalidQueue,
                        consumerConfig.command.exchange,
                        consumerConfig.command.invalidRoutingKey
                    );
                }

                if (consumerConfig.command.backlogQueue) {
                    await channel.assertQueue(consumerConfig.command.backlogQueue, {
                        exclusive: false,
                        durable: true,
                        autoDelete: false
                    });
                    await channel.bindQueue(
                        consumerConfig.command.backlogQueue,
                        consumerConfig.command.exchange,
                        consumerConfig.command.backlogRoutingKey
                    );
                }

                if (consumerConfig.command.delayedQueue) {
                    await channel.assertQueue(consumerConfig.command.delayedQueue, {
                        exclusive: false,
                        durable: true,
                        autoDelete: false,
                        arguments: {
                            'x-message-ttl': consumerConfig.command.defaultDelay,
                            'x-dead-letter-exchange': consumerConfig.command.exchange,
                            'x-dead-letter-routing-key': consumerConfig.command.backlogRoutingKey
                        }
                    });
                    await channel.bindQueue(
                        consumerConfig.command.delayedQueue,
                        consumerConfig.command.exchange,
                        consumerConfig.command.delayedRoutingKey
                    );
                }
            }
            if (consumerConfig.query) {
                logger().info('setup the query AMQP API');
                await channel.assertExchange(consumerConfig.query.exchange, 'direct', {
                    durable: true
                });
                await channel.assertQueue(consumerConfig.query.backlogQueue, {
                    exclusive: false,
                    durable: true,
                    autoDelete: false
                });
                await channel.bindQueue(
                    consumerConfig.query.backlogQueue,
                    consumerConfig.query.exchange,
                    consumerConfig.query.backlogRoutingKey
                );
                await channel.assertQueue(consumerConfig.query.invalidQueue, {
                    exclusive: false,
                    durable: true,
                    autoDelete: false
                });
                await channel.bindQueue(
                    consumerConfig.query.invalidQueue,
                    consumerConfig.query.exchange,
                    consumerConfig.query.invalidRoutingKey
                );
            }
        }

        if (cnqConfig.producer && cnqConfig.producer.amqp) {
            logger().info('setup the reply AMQP API');
            await amqp.channel.assertQueue(amqp.replyToQueue, {
                exclusive: true,
                durable: false,
                autoDelete: false
            });
        }
    }

    return amqp;
}
