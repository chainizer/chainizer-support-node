import {Message} from 'amqplib';
import {Options} from 'amqplib/properties';
import {kebabCase, merge, omit} from 'lodash';
import {v4 as uuid} from 'uuid';
import {Metadata, taskLogger, TaskType} from '@chainizer/support-executor';
import {config} from './config';
import {getCnqAmqp} from './amqp';

function fromMetadataToHeaders(metadata: Metadata): any {
    return merge(
        Object.keys(omit(metadata, 'headers')).reduce((o, k) => {
            o['executor-' + kebabCase(k)] = metadata[k];
            return o;
        }, {}),
        Object.keys(metadata.headers || {}).reduce((o, k) => {
            o[kebabCase(k)] = metadata.headers[k];
            return o;
        }, {})
    );
}

export class TaskReplyTimeout extends Error {
    public status = 500;
    public name = 'TaskReplyTimeout';

    constructor(type: string, name: string) {
        super(`the task ${type}/${name} failed because timeout has been reached`);
    }
}

export class TaskFailedWrapper extends Error {
    constructor(
        public status: number,
        public type: string,
        public name: string,
        public causeName: string,
        public causeMessage: string
    ) {
        super(`the task ${type}/${name} failed: ${causeName} - ${causeMessage}`);
    }
}

export class TargetNotConfigured extends Error {
    public status = 500;
    public name = 'TargetNotConfigured';

    constructor(target: string, path: string) {
        super(`the target ${target} is not configured: ${path}`);
    }
}

export async function dispatchTask(
    target: string,
    type: TaskType,
    name: string,
    payload: any = undefined,
    metadata: Metadata = {},
    wait: boolean = true
): Promise<any> {
    const amqp = await getCnqAmqp();

    if (!metadata.messageId) {
        metadata.messageId = uuid()
    }
    const messageId = metadata.messageId;
    const options: Options.Publish = {
        messageId,
        type: name,
        appId: amqp.appId,
        contentType: 'application/json',
        contentEncoding: 'utf8',
        headers: fromMetadataToHeaders(metadata)
    };
    if (wait) {
        options.replyTo = amqp.replyToQueue;
    }
    const content = new Buffer(payload ? JSON.stringify(payload) : '');
    const L = taskLogger(metadata);

    L.debug('dispatch', type, name, amqp.replyToQueue);

    if (!config().get(`cnq.producer.amqp.${type}.${target}`)) {
        throw new TargetNotConfigured(target, `executor.producer.amqp.${type}.${target}`);
    }

    amqp.channel.publish(
        config().cnq.producer.amqp[type][target].exchange,
        config().cnq.producer.amqp[type][target].routingKey,
        content,
        options
    );

    if (!wait) {
        return Promise.resolve();
    }

    return new Promise((resolve, reject) => {
        let timeoutId;
        let consumer;

        amqp.channel.consume(amqp.replyToQueue, async (message: Message) => {
            if (message && messageId === message.properties.correlationId) {
                clearTimeout(timeoutId);
                amqp.channel.ack(message, false);
                L.debug('reply received', type, name, message.properties.correlationId, amqp.replyToQueue);
                await amqp.channel.cancel(consumer.consumerTag);
                const status = parseInt(message.properties.headers['executor-status']);
                let content;
                if (message.content && message.content.toString()) {
                    content = JSON.parse(message.content.toString(message.properties.contentEncoding));
                }
                if (status >= 200 && status < 300) {
                    resolve(content);
                } else {
                    reject(new TaskFailedWrapper(status || 500, type, name, content.name, content.message));
                }
            }
        }).then(c => consumer = c);

        timeoutId = setTimeout(async () => {
            try {
                await amqp.channel.cancel(consumer.consumerTag);
            } catch (e) {
                L.warn('reply not received');
            } finally {
                reject(new TaskReplyTimeout(type, name));
            }
        }, config().cnq.producer.timeout || 1000 * 60 * 60);
    });
}
