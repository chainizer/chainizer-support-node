import 'mocha';
import * as request from 'supertest';
import {expect} from 'chai';
import {cleanRegisteredTasks, registerTask, TaskType} from '@chainizer/support-executor';
import {configureRestConsumer} from '../src/rest';

describe('rest', () => {
    let app;

    before(() => {
        app = configureRestConsumer();
        cleanRegisteredTasks();
        registerTask(TaskType.command, 'test-ok', (payload, metadata) => {
            return Promise.resolve({payload, metadata});
        });
        registerTask(TaskType.query, 'test-ok', (payload, metadata) => {
            return Promise.resolve({payload, metadata});
        });
    });

    it('should handle a command', async () => {
        await request(app)
            .post('/api/command/test-ok')
            .type('json')
            .set('Executor-Key1', 'value1')
            .set('Authorization', 'Bearer atoken')
            .send({
                key1: 'value1'
            })
            .expect(200)
            .expect('Content-Type', /json/)
            .expect(res => {
                expect(res.body).to.have.nested.property('payload.key1', 'value1');
                expect(res.body).to.have.nested.property('metadata.key1', 'value1');
                expect(res.body).to.have.nested.property('metadata.headers.authorization', 'Bearer atoken');
            });
    });

    it('should failed when command not found', async () => {
        await request(app)
            .post('/api/command/test-not-found')
            .type('json')
            .send({
                key1: 'value1'
            })
            .expect(404)
            .expect('Content-Type', /json/)
            .expect(res => {
                expect(res.body).to.have.property('name', 'TaskNotFound');
                expect(res.body).to.have.property('message', 'unable to find the task command/test-not-found');
            });
    });

    it('should handle a query', async () => {
        await request(app)
            .get('/api/query/test-ok')
            .type('json')
            .set('Executor-Key1', 'value1')
            .query({
                key1: 'value1'
            })
            .expect(200)
            .expect('Content-Type', /json/)
            .expect(res => {
                expect(res.body).to.have.nested.property('payload.key1', 'value1');
                expect(res.body).to.have.nested.property('metadata.key1', 'value1');
            });
    });

    it('should failed when query not found', async () => {
        await request(app)
            .get('/api/query/test-not-found')
            .type('json')
            .send({
                key1: 'value1'
            })
            .expect(404)
            .expect('Content-Type', /json/)
            .expect(res => {
                expect(res.body).to.have.property('name', 'TaskNotFound');
                expect(res.body).to.have.property('message', 'unable to find the task query/test-not-found');
            });
    });

});