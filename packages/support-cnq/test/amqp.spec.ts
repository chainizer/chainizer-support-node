import {Channel, Connection} from 'amqplib';
import {expect} from 'chai';
import 'mocha';
import {Amqp, getCnqAmqp, setCnqAmqp} from '../src/amqp';
import {configureAmqpConsumer} from '../src/consumer';
import {cleanRegisteredTasks, registerTask, TaskType} from '@chainizer/support-executor';
import {registerConfig} from '@chainizer/support-config';
import {dispatchTask} from '../src/producer';
import {config} from '../src/config';

registerConfig(__dirname + '/../config/testing.config.json');

describe('amqp', () => {

    before(async () => {
        cleanRegisteredTasks();
        registerTask(TaskType.command, 'test-ok', (payload, metadata) => {
            return Promise.resolve({payload, metadata});
        });
        registerTask(TaskType.command, 'test-exception', () => {
            return Promise.reject(new Error('an error'));
        });
        registerTask(TaskType.command, 'test-too-long', () => {
            return new Promise(resolve => setTimeout(() => resolve(), 1500));
        });
        registerTask(TaskType.query, 'test-ok', (payload, metadata) => {
            return Promise.resolve({payload, metadata});
        });
        const amqp = await configureAmqpConsumer();
        await amqp.channel.purgeQueue(amqp.replyToQueue);
        await amqp.channel.purgeQueue(config().cnq.consumer.amqp.command.backlogQueue);
        await amqp.channel.purgeQueue(config().cnq.consumer.amqp.command.delayedQueue);
        await amqp.channel.purgeQueue(config().cnq.consumer.amqp.command.invalidQueue);
        await amqp.channel.purgeQueue(config().cnq.consumer.amqp.query.backlogQueue);
        await amqp.channel.purgeQueue(config().cnq.consumer.amqp.query.invalidQueue);
    });

    after((done) => {
        setTimeout(async () => {
            try {
                const amqp = await getCnqAmqp();
                await amqp.destroy();
            } catch (e) {
                console.log('unable to close channel');
            } finally {
                done();
            }
        }, 1000);
    });

    it('should set amqp', async () => {
        const amqp = await getCnqAmqp();
        const anotherAmqp = new Amqp(<Connection> {}, <Channel> {});
        setCnqAmqp(anotherAmqp);
        expect(await getCnqAmqp()).to.be.equal(anotherAmqp);
        setCnqAmqp(amqp);
        expect(await getCnqAmqp()).to.be.equal(amqp);
    });

    it('should ping the amqp configuration', async () => {
        const amqp = await getCnqAmqp();
        await amqp.ping();
    });

    it('should handle a command', async () => {
        const result = await dispatchTask(
            'default',
            TaskType.command,
            'test-ok',
            {key1: 'value1'},
            {key1: 'value1', headers: {xKey: 'xValue'}}
        );
        expect(result).to.have.nested.property('payload.key1', 'value1');
        expect(result).to.have.nested.property('metadata.key1', 'value1');
        expect(result).to.have.nested.property('metadata.headers.xKey', 'xValue');
    });

    it('should handle a command without payload', async () => {
        const result = await dispatchTask(
            'default',
            TaskType.command,
            'test-ok',
            undefined,
            {key1: 'value1', headers: {xKey: 'xValue'}}
        );
        expect(result).to.have.nested.property('payload', '');
        expect(result).to.have.nested.property('metadata.key1', 'value1');
        expect(result).to.have.nested.property('metadata.headers.xKey', 'xValue');
    });

    it('should handle a command with an empty payload', async () => {
        const result = await dispatchTask(
            'default',
            TaskType.command,
            'test-ok',
            '',
            {messageId: 'aMessage'}
        );
        expect(result).to.have.nested.property('payload', '');
    });

    it('should handle a command without wait', async () => {
        const result = await dispatchTask(
            'default',
            TaskType.command,
            'test-ok',
            {key1: 'value1'},
            {key1: 'value1'},
            false
        );
        expect(result).to.not.exist;
    });

    it('should handle a query', async () => {
        const result = await dispatchTask(
            'default',
            TaskType.query,
            'test-ok',
            {key1: 'value1'},
            {key1: 'value1'}
        );
        expect(result).to.have.nested.property('payload.key1', 'value1');
        expect(result).to.have.nested.property('metadata.key1', 'value1');
    });

    it('should handle exception', async () => {
        try {
            const result = await dispatchTask(
                'default',
                TaskType.command,
                'test-exception',
                {key1: 'value1'},
                {key1: 'value1'}
            );
            expect(result).to.be.null;
        } catch (e) {
            expect(e).to.have.property('status', 500);
            expect(e).to.have.property('causeName', 'TaskFailed');
            expect(e).to.have.property('causeMessage', 'the task command/test-exception failed because Error/an error');
        }
    });

    it('should handle not found', async () => {
        try {
            const result = await dispatchTask(
                'default',
                TaskType.command,
                'test-not-found',
                {key1: 'value1'},
                {key1: 'value1'}
            );
            expect(result).to.be.null;
        } catch (e) {
            expect(e).to.have.property('status', 404);
            expect(e).to.have.property('causeName', 'TaskNotFound');
            expect(e).to.have.property('causeMessage', 'unable to find the task command/test-not-found');
        }
    });

    it('should handle timeout', async () => {
        try {
            const result = await dispatchTask(
                'default',
                TaskType.command,
                'test-too-long'
            );
            expect(result).to.be.null;
        } catch (e) {
            expect(e).to.have.property('status', 500);
            expect(e).to.have.property('name', 'TaskReplyTimeout');
            expect(e).to.have.property('message', 'the task command/test-too-long failed because timeout has been reached');
        }
    });

    it('should handle not configured target', async () => {
        try {
            const result = await dispatchTask(
                'unknown',
                TaskType.command,
                'a-command'
            );
            expect(result).to.be.null;
        } catch (e) {
            expect(e).to.have.property('status', 500);
            expect(e).to.have.property('name', 'TargetNotConfigured');
            expect(e).to.have.property('message', 'the target unknown is not configured: executor.producer.amqp.command.unknown');
        }
    });

});
