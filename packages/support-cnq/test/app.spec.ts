import 'mocha';
import {getHooks, Hook, startApp} from '@chainizer/support-app';
import {config} from '../src/config';
import '../src/main';
import {registerConfig} from '@chainizer/support-config';

registerConfig(__dirname + '/../config/testing.config.json');
config().set('winston.default.console', true);

describe('app', () => {

    it('should configure rest and amqp', done => {
        config().set('winston.default.console', true);
        startApp().then(async server => {

            for(const hook of getHooks().health) {
                await hook();
            }

            for(const hook of getHooks().shutdown) {
                await hook();
            }

            server.close(done);
        });


    });

});