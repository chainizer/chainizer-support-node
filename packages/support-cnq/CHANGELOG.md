# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.0.0-alpha.55"></a>
# [1.0.0-alpha.55](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.54...v1.0.0-alpha.55) (2018-08-14)




**Note:** Version bump only for package @chainizer/support-cnq

<a name="1.0.0-alpha.54"></a>
# [1.0.0-alpha.54](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.53...v1.0.0-alpha.54) (2018-08-03)


### Features

* upgrade dependencies ([7f89317](https://gitlab.com/chainizer/chainizer-support-node/commit/7f89317))




<a name="1.0.0-alpha.53"></a>
# [1.0.0-alpha.53](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.52...v1.0.0-alpha.53) (2018-06-04)




**Note:** Version bump only for package @chainizer/support-cnq

<a name="1.0.0-alpha.51"></a>
# [1.0.0-alpha.51](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.50...v1.0.0-alpha.51) (2018-05-31)


### Features

* rename config stuff ([5373088](https://gitlab.com/chainizer/chainizer-support-node/commit/5373088))




<a name="1.0.0-alpha.50"></a>
# [1.0.0-alpha.50](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.49...v1.0.0-alpha.50) (2018-05-29)




**Note:** Version bump only for package @chainizer/support-cnq

<a name="1.0.0-alpha.44"></a>
# [1.0.0-alpha.44](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.43...v1.0.0-alpha.44) (2018-05-25)


### Bug Fixes

* start comsumer only if backlog queue is configured ([5df97d9](https://gitlab.com/chainizer/chainizer-support-node/commit/5df97d9))




<a name="1.0.0-alpha.43"></a>
# [1.0.0-alpha.43](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.42...v1.0.0-alpha.43) (2018-05-25)


### Bug Fixes

* AMQP command consumers can declare backlog and/or delayed command queues ([e7b17d8](https://gitlab.com/chainizer/chainizer-support-node/commit/e7b17d8))




<a name="1.0.0-alpha.42"></a>
# [1.0.0-alpha.42](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.41...v1.0.0-alpha.42) (2018-05-25)


### Bug Fixes

* fix management of consumed answers ([e5987a0](https://gitlab.com/chainizer/chainizer-support-node/commit/e5987a0))




<a name="1.0.0-alpha.41"></a>
# [1.0.0-alpha.41](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.40...v1.0.0-alpha.41) (2018-05-25)


### Bug Fixes

* add getCnqAmqp to main api ([b50f5ec](https://gitlab.com/chainizer/chainizer-support-node/commit/b50f5ec))




<a name="1.0.0-alpha.38"></a>
# [1.0.0-alpha.38](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.37...v1.0.0-alpha.38) (2018-05-24)


### Bug Fixes

* fix broken unit test ([b32b946](https://gitlab.com/chainizer/chainizer-support-node/commit/b32b946))


### Features

* expose Amqp and setCnqAmqp directly from main ([2532250](https://gitlab.com/chainizer/chainizer-support-node/commit/2532250))




<a name="1.0.0-alpha.37"></a>
# [1.0.0-alpha.37](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.36...v1.0.0-alpha.37) (2018-05-23)


### Features

* handle headers when dealing from AMQP/HTTP requests and task metadata ([1750eb4](https://gitlab.com/chainizer/chainizer-support-node/commit/1750eb4))




<a name="1.0.0-alpha.36"></a>
# [1.0.0-alpha.36](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.35...v1.0.0-alpha.36) (2018-05-23)


### Bug Fixes

* be sure cnq can start even without a proper producer or consumer ([b8d3623](https://gitlab.com/chainizer/chainizer-support-node/commit/b8d3623))




<a name="1.0.0-alpha.35"></a>
# [1.0.0-alpha.35](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.34...v1.0.0-alpha.35) (2018-05-23)


### Bug Fixes

* cnq cannot start if when amqp consumer are not defined ([f48215f](https://gitlab.com/chainizer/chainizer-support-node/commit/f48215f))




<a name="1.0.0-alpha.34"></a>
# [1.0.0-alpha.34](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.33...v1.0.0-alpha.34) (2018-05-23)




**Note:** Version bump only for package @chainizer/support-cnq

<a name="1.0.0-alpha.33"></a>
# [1.0.0-alpha.33](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.32...v1.0.0-alpha.33) (2018-05-23)




**Note:** Version bump only for package @chainizer/support-cnq

<a name="1.0.0-alpha.32"></a>
# [1.0.0-alpha.32](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.31...v1.0.0-alpha.32) (2018-05-22)




**Note:** Version bump only for package @chainizer/support-cnq

<a name="1.0.0-alpha.31"></a>
# [1.0.0-alpha.31](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.30...v1.0.0-alpha.31) (2018-05-22)




**Note:** Version bump only for package @chainizer/support-cnq

<a name="1.0.0-alpha.30"></a>
# [1.0.0-alpha.30](https://gitlab.com/chainizer/chainizer-support-node/compare/v1.0.0-alpha.29...v1.0.0-alpha.30) (2018-05-22)


### Features

* add CNQ integration ([910878c](https://gitlab.com/chainizer/chainizer-support-node/commit/910878c))
